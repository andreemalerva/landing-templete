<div>
    <h1>Templete Landing Page</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este sitio esa hecho con la ayuda de tecnologías como lo son HTML, CSS y Javascript. 🫶🏻

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://andreemalerva.gitlab.io/landing-templete/)😎

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de © 2023 LYAM ANDREE CRUZ MALERVA
